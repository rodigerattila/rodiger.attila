<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Http\Requests\FlickrPhotoRequest;
use App\Http\Requests\FlickrSearchRequest;
use App\Repositories\FlickrRepository;

class FlickrController extends Controller
{
    /**
     * @var FlickrRepository
     */
    protected $flickr;
    protected $lists;

    /**
     * Initialize Flickr API using DI approach.
     *
     * FlickrController constructor.
     * @param FlickrRepository $flickr
     */
    public function __construct(FlickrRepository $flickr)
    {
        $this->flickr = $flickr;
        $this->setList();           

    }

    /**
     * Display search page.
     *
     * @throws \Exception
     */
    public function index()
    {
        $this->flickr->testFlickrApi();
        return view('flickr.index')->with('lists', $this->lists);
    }

    /**
     * Search photos
     *
     * @param FlickrSearchRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function search(FlickrSearchRequest $request)
    {

        $photos = $this->flickr->searchPhotos($request, 20);

        if ( $photos ) {

            return view('flickr.search', compact('photos'));    

        } else {

            return view('flickr.noresult');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(FlickrPhotoRequest $request, $id)
    {
        $photo = $this->flickr->findOrThrowException($id);

        return view('flickr.photo', compact('photo'));
    }

    public function setList($array=Array()) {

        $defalut_list_array = new \stdClass ();

        $defalut_list_array->data = (object) Array(
            Array('nev' => 'Animals'),
            Array('nev' => 'Food'),
            Array('nev' => 'Vechicle'),
            Array('nev' => 'Movie'),
        );

        // Animals
        $defalut_list_array->Animals = (object) Array(
            Array('nev' => 'Pets', 'sign' => 'pets'),
            Array('nev' => 'Wild animals', 'sign' => 'wildanimals' ),
            Array('nev' => 'Domestic animals', 'sign' => 'domesticanimals')
        );

        $defalut_list_array->pets = (object) Array(
            Array('nev' => 'Guppy'),
            Array('nev' => 'Parrot'),
            Array('nev' => 'GoldFish'),
            Array('nev' => 'Dog'),
            Array('nev' => 'Cat'),
        );        

        $defalut_list_array->wildanimals = (object) Array(
            Array('nev' => 'Tiger'),
            Array('nev' => 'Ant'),
            Array('nev' => 'Tetra'),
            Array('nev' => 'Peafowl'),
            Array('nev' => 'Mongoose'),
        );   

        $defalut_list_array->domesticanimals = (object) Array(
            Array('nev' => 'Cow'),
            Array('nev' => 'Pig'),
            Array('nev' => 'Goat'),
            Array('nev' => 'Horse'),
        );        

        // Food
        $defalut_list_array->Food = (object) Array(
            Array('nev' => 'Fast food', 'sign' => 'fastfood'),
            Array('nev' => 'Dessert', 'sign' => 'dessert' ),
        );

        $defalut_list_array->fastfood = (object) Array(
            Array('nev' => 'Cheeseburger'),
            Array('nev' => 'Hamburger'),
        );        

        $defalut_list_array->dessert = (object) Array(
            Array('nev' => 'Chocolate'),
            Array('nev' => 'Cookie'),
            Array('nev' => 'Cake'),
            Array('nev' => 'Pie'),
        );   


        // Vechicle
        $defalut_list_array->Vechicle = (object) Array(
            Array('nev' => 'Motorcycle', 'sign' => 'motorcycle'),
            Array('nev' => 'Car', 'sign' => 'car' ),
        );

        $defalut_list_array->motorcycle = (object) Array(
            Array('nev' => 'Harley Davidson'),
        );        

        $defalut_list_array->car = (object) Array(
            Array('nev' => 'Lamborghini'),
            Array('nev' => 'Ferrari'),
            Array('nev' => 'Bugatti'),
            Array('nev' => 'BMW'),
            Array('nev' => 'Mercedes')
        );   


        // Movie
        $defalut_list_array->Movie = (object) Array(
            Array('nev' => 'Science fiction', 'sign' => 'sciencefiction'),
        );

        $defalut_list_array->sciencefiction = (object) Array(
            Array('nev' => 'Sunshine'),
            Array('nev' => 'Interstellar'),
            Array('nev' => 'The Moon'),
            Array('nev' => 'Oblivion'),
            Array('nev' => 'Star Trek'),
            Array('nev' => 'Star Wars'),
        );        

        $this->lists = $defalut_list_array;


    }

}
