$(document).ready(function(){
$('select[name="searchlist"]').change(function(e) {
	var inputUserRoles = $(this).val();

	if ( inputUserRoles == 'empty' ) {
		$('input[name="search"]').show();
	} else {
	$('input[name="search"]').val('');	
	$('input[name="search"]').hide();

	}
});

$('input[name="search"]').keyup(function () {
	$('select[name="searchlist"]').val('empty');
});

});