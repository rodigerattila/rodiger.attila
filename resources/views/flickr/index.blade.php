@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="jumbotron mt-5">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="container">
                <div class="links">
                    <form method="GET" action="/flickr/search" name="form1">
                    @csrf
                    <div class="form-group input-group-lg">
                        <input type="text" class="form-control" name="search" placeholder="Keyword ..." />
                    </div>
 
                    <div class="form-group input-group-lg">
                        <select name="searchlist" class="form-control">
                        <option value="empty">Or choose from categories</option>
                        @foreach ($lists->data as $list)
                            <optgroup label="{{$list['nev']}}">
                            @foreach ($lists->{$list['nev']} as $list2)
                                <option value="{{$list2['nev']}}">{{$list2['nev']}}</option>
                                @foreach ($lists->{$list2['sign']} as $list3)
                                    <option value="{{$list3['nev']}}">--- {{$list3['nev']}}</option>
                                @endforeach                                
                            @endforeach
                        @endforeach 
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg">Search</button>
                    </form>                  
                </div>
            </div>
        </div>
    </div>
@endsection
 