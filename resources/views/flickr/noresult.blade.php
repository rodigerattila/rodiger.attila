@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="text-right"><a class="btn btn-primary btn-lg" href="{{ route('flickr.index') }}" role="button">Új keresés indítása</a></div>
        <div class="heading">
            <h1 class="text-center">No result!</h1>
        </div>
        
    </div>
@endsection