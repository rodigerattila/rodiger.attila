@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="text-right"><a class="btn btn-primary btn-lg" href="{{ URL::previous() }}" role="button">Back to search result</a></div>

        <div class="card-columns">

                <div class="card">
                    <img class="card-img-top" src="{{ $photo['url'] }}" alt="{{ $photo['title'] }}">
                    <div class="card-body">
                        <h3 class="card-title text-center">{{ $photo['title'] }}</h3>
                        <p>{{ $photo['description'] }}</p>
                    </div>
                </div>
        </div>  
       
    </div>
@endsection
