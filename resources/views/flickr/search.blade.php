@extends('layouts.master')

@section('content')

    <div class="container">
        <div class="text-right"><a class="btn btn-primary btn-lg" href="{{ route('flickr.index') }}" role="button">New search</a></div>
        <div class="heading">
            <h1 class="text-center">Search keyword:  
                @if (app('request')->input('search'))
                    {{ app('request')->input('search') }}
                @else 
                    {{ app('request')->input('searchlist') }}
                @endif 
        
            </h1>
        </div>
        <div class="card-columns">
        @foreach(array_chunk($photos->all(), 5) as $threePhotos)
        
            @foreach($threePhotos as $photo) 
                <div class="card">
                    <a href="{{ $photo['id'] }}" title="{{ $photo['title'] }}"><img class="card-img-top" src="{{ $photo['url'] }}" alt="{{ $photo['title'] }}">
                    <div class="card-body">
                        <h3 class="card-title text-center"><a href="{{ $photo['id'] }}" title="{{ $photo['title'] }}">{{ $photo['title'] }}</a></h3>
                    </div>
                </div>
            @endforeach
        
        @endforeach
        </div>  
        <div class="text-center">{!! $photos->withPath(Request::url())->appends(request()->query())->links() !!}</div>
    </div>
@endsection